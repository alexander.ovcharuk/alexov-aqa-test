# QA AUTO TEST ASSIGNMENT 

**List of refactored items:**

1. Removed Gradle
2. Removed not used models, folders, files
3. Optimized POM
4. Fixed failed tests
5. Updated libraries
6. Updated Java version
7. Moved base `service.url` to `serenity.conf`. It's better to keep such properties there, and it's only 1 place where it's mentioned
8. Implemented scenarios using Cucumber Examples tables
9. Positive and negative scenarios were written

*The project directory structure*
The project has build scripts for Maven, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                      Test runners and supporting code
    + resources
      + features                Feature files
      + search                  Feature file subdirectories 
```

*Perform test run:*

1. Install JAVA 
2. Install Maven 
3. Clone project URL: https://gitlab.com/alexander.ovcharuk/alexov-aqa-test.git
4. Install dependencies: `mvn clean install -DskipTests=true`
5. Run test: `mvn verify`


*Generating the reports*
To Generate the reports use command:
`mvn serenity:aggregate`
XML report is located in `target/surefire-reports/*.xml`
HTML report is located in `target/site/serenity/index.html`


*Gitlab CI/CD*
Gitlab CICD is setup to run testcases. 
Pipeline to run tests: https://gitlab.com/alexander.ovcharuk/alexov-aqa-test/-/pipelines
Job archives the html and xml report artifacts

